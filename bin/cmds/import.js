const chalk = require("chalk");
const replacer = require("../lib/replacer");
const spinner = require("../lib/spinner");
const checks = require("../lib/checks");
const consts = require("../lib/consts");
const paths = require("../lib/paths");
const npm = require("../lib/npm");
const disk = require("../lib/disk");

module.exports = async args => {
  try {
    console.log(`Importing ${chalk.greenBright(args.package)}\n`);

    const dir = paths.componentDir(args.package);

    if (await checks.alreadyImported(args.package)) {
      return;
    }

    await disk.ensureComponentsFolderExists();
    await disk.copyNpmPackageToComponentFolder(args.package);
    await replacer.replaceToFile(args.package, dir);
    spinner.start("Installing npm packages..");
    await npm.install();
    spinner.stop();
    console.log(`  Imported to ${chalk.bold(consts.DESTINATION_DIR)}`);
    process.stdout.write("\n");
  } catch (err) {
    spinner.stop();
    console.error(chalk.redBright(err));
  }
};
