const chalk = require("chalk");
const fse = require("fs-extra");
const paths = require("../lib/paths");

module.exports = async args => {
  try {
    console.log(
      `Creating ${chalk.greenBright(args.package)} component skeleton\n`
    );

    const dir = paths.componentDir(args.package);

    await fse.ensureDir(dir);

    const json = await fse.readJson(`${__dirname}/../templates/package.json`);
    json.name = args.package;
    await fse.writeJson(`${dir}/package.json`, json, { spaces: 2 });

    await fse.copy(`${__dirname}/../templates/index.js`, `${dir}/index.js`);

    console.log(
      `  Component ${chalk.bold(args.package)} created in ${chalk.bold(dir)}`
    );
    process.stdout.write("\n");
  } catch (err) {
    console.error(chalk.redBright(err));
  }
};
