const chalk = require("chalk");
const npm = require("../lib/npm");
const spinner = require("../lib/spinner");

module.exports = async args => {
  try {
    console.log(`Publishing ${chalk.greenBright(args.package)}`);

    spinner.start("Publishing npm package..");
    await npm.publish(args.package);
    spinner.stop();
    console.log(`  ${chalk.bold(args.package)} was published`);
    process.stdout.write("\n");
  } catch (err) {
    spinner.stop();
    console.error(chalk.redBright(err));
  }
};
