const chalk = require("chalk");
const replacer = require("../lib/replacer");

module.exports = async args => {
  try {
    console.log(`Tagging ${chalk.greenBright(args.package)}\n`);
    await replacer.cleanPackageJson(args.package);
    await replacer.replaceComponentToVersion(args.package, args.vers_ion);
    console.log(`  Tagged as ${chalk.bold.magenta(args.vers_ion)}`);
    process.stdout.write("\n");
  } catch (err) {
    console.error(chalk.redBright(err));
  }
};
