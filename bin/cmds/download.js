const chalk = require("chalk");
const fse = require("fs-extra");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const paths = require("../lib/paths");

module.exports = async args => {
  console.log(
    `Downloading ${chalk.greenBright(args.package)} component source code\n`
  );

  const name = paths.cropOrgName(args.package);

  const fileName = (await exec(`npm pack ${args.package}`)).stdout;
  fse.ensureDir(name.packageName);

  await exec(`tar -xzf ${fileName}`);

  await exec(`mv ./package/* ${name.packageName}`);
  await exec(`rm ${fileName}`);

  await fse.remove("./package");

  console.log(
    `  Component ${chalk.bold(args.package)} downloaded to ${chalk.bold(
      name.packageName
    )}`
  );
  process.stdout.write("\n");
};
