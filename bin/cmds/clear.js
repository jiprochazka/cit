const chalk = require("chalk");
const spinner = require("../lib/spinner");
const replacer = require("../lib/replacer");
const npm = require("../lib/npm");
const disk = require("../lib/disk");

module.exports = async args => {
  try {
    console.log(`Clearing ${chalk.greenBright(args.package)}\n`);

    disk.removeNpmPackageFolder(args.package);

    spinner.start("Installing npm packages..");
    const version = await npm.getLatestVersion(args.package);

    await replacer.replaceProjectToVersion(args.package, version);

    await npm.install();
    spinner.stop();

    await disk.removeComponentFolder(args.package);

    console.log(`  Project ${chalk.bold(args.package)} cleared`);
    process.stdout.write("\n");
  } catch (err) {
    spinner.stop();
    console.error(chalk.redBright(err));
  }
};
