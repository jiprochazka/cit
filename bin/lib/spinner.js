const Spinner = require("cli-spinner").Spinner;
const spinner = new Spinner("");

exports.start = text => {
  if (text) {
    spinner.setSpinnerTitle(text);
  }
  spinner.setSpinnerString(21);
  spinner.start();
};

exports.stop = () => {
  spinner.stop();
  process.stdout.write("\n");
};
