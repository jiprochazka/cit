const replace = require("replace-in-file");
const chalk = require("chalk");
const fse = require("fs-extra");
const consts = require("../lib/consts");
const paths = require("../lib/paths");

exports.replaceToFile = async (packageName, path) => {
  console.log(consts.PACKAGE_JSON_FILE);
  console.log(packageName);
  console.log(path);
  await doReplace(
    consts.PACKAGE_JSON_FILE,
    packageName,
    `file:${path}`,
    "Error in package.json replacement (version -> file) occurred:"
  );
};

exports.replaceProjectToVersion = async (packageName, version) => {
  await doReplace(
    consts.PACKAGE_JSON_FILE,
    packageName,
    version,
    `Error in package.json replacement (file -> version '${version}') occurred:`
  );
};

exports.replaceComponentToVersion = async (packageName, version) => {
  await doReplace(
    `${paths.componentDir(packageName)}/package.json`,
    "version",
    version,
    `Error in package.json replacement (file -> version '${version}') occurred:`
  );
};

exports.cleanPackageJson = async packageName => {
  const json = await fse.readJson(
    `${consts.DESTINATION_DIR}/${packageName}/package.json`
  );

  const content = {
    main: json.main,
    name: json.name,
    version: json.version
  };

  await fse.writeJson(
    `${consts.DESTINATION_DIR}/${packageName}/package.json`,
    content,
    { spaces: 2 }
  );
};

async function doReplace(file, packageName, to, errorMessage) {
  try {
    var regexp = new RegExp(`"${packageName}": "(.*)"`, "g");
    const options = {
      files: file,
      from: regexp,
      to: `"${packageName}": "${to}"`
    };
    await replace(options);
  } catch (error) {
    console.log(chalk.redBright.bold(errorMessage, error));
  }
}
