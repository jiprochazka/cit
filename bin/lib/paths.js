const consts = require("../lib/consts");

exports.componentDir = packageName => {
  return `${consts.DESTINATION_DIR}/${packageName}`;
};

exports.cropOrgName = packageName => {
  const parts = packageName.split("/");
  if (parts.length > 1) {
    return { org: parts[0], packageName: parts[1] };
  }
  return { org: null, packageName: parts[0] };
};
