const util = require("util");
const exec = util.promisify(require("child_process").exec);

exports.install = async () => {
  await exec("npm i");
};

exports.getLatestVersion = async packageName => {
  return (await exec(`npm show ${packageName} version`)).stdout.replace(
    /\n/g,
    ""
  );
};

exports.publish = async packageName => {
  await exec(`cd ./components/${packageName} && npm publish`);
};
