const chalk = require("chalk");
const fse = require("fs-extra");
const consts = require("../lib/consts");

exports.checkIsInProjectRoot = async () => {
  if (!(await fse.pathExists(consts.PACKAGE_JSON_FILE))) {
    console.log(chalk.gray("It looks like you are not in a project root!"));
  }
};

exports.alreadyImported = async packageName => {
  const result = await fse.pathExists(
    `${consts.DESTINATION_DIR}/${packageName}`
  );
  if (result) {
    console.log(chalk.inverse(`Package '${packageName}' is already imported!`));
  }

  return result;
};
