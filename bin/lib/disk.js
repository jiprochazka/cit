const fse = require("fs-extra");
const paths = require("./paths");
const consts = require("./consts");

exports.removeComponentFolder = async packageName => {
  const dir = paths.componentDir(packageName);

  await fse.remove(dir);

  let destinationDir = await fse.readdir(consts.DESTINATION_DIR);

  destinationDir.forEach(async dir => {
    const subdirPath = `${consts.DESTINATION_DIR}/${dir}`;
    const subdir = await fse.readdir(subdirPath);
    if (subdir.length === 0) {
      await fse.remove(subdirPath);
    }
  });

  destinationDir = await fse.readdir(consts.DESTINATION_DIR);
  if (destinationDir.length === 0) {
    await fse.remove(consts.DESTINATION_DIR);
  }
};

exports.removeNpmPackageFolder = async packageName => {
  await fse.remove(`./node_modules/${packageName}`);
};

exports.copyNpmPackageToComponentFolder = async packageName => {
  const dir = paths.componentDir(packageName);
  await fse.copy(`./node_modules/${packageName}`, dir);
};

exports.ensureComponentsFolderExists = async () => {
  await fse.ensureDir(consts.DESTINATION_DIR);
};
