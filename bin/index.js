#!/usr/bin/env node
const yargs = require("yargs");
const chalk = require("chalk");
const checks = require("./lib/checks");

(async function() {
  const cmd = yargs
    .command("import <package>", "Imports component")
    .command("tag <vers_ion> <package>", "Taggs version")
    .command("publish <package>", "Publishes component")
    .command("create <package>", "Creates new component skeleton")
    .command("clear <package>", "Sets project to be loaded pro npm package")
    .command("download <package>", "Downloads source code from NPM")
    .help().argv;
  await checks.checkIsInProjectRoot();
  switch (cmd._[0]) {
    case "import":
      require("./cmds/import")(cmd);
      break;
    case "tag":
      require("./cmds/tag")(cmd);
      break;
    case "publish":
      require("./cmds/publish")(cmd);
      break;
    case "create":
      require("./cmds/create")(cmd);
      break;
    case "clear":
      require("./cmds/clear")(cmd);
      break;
    case "download":
      require("./cmds/download")(cmd);
      break;
    default:
      console.error(
        chalk.bgRedBright.bold(`"${cmd._[0]}" is not a valid command!`)
      );
      break;
  }
})();
