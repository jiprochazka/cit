# Intro

As we are not allowed to use [bit.dev](http://bit.dev/) in corporate, this is a lightweight replacement of it.  
It is built in [Node.js](https://nodejs.org), so you must have Node.js installed on the machine.

**It is crucial to execute all the commands from the project root folder (where the its package.json file is)!!**

### Legend:

**Project** - the project which you are importing the components **to**  
**Component** - a npm package with Vue components and other shared javascript code.

# Installation

Go to the root folder of this package and run `npm link` to link the bin folder to nodejs command. Now you are able to call `cit <command>` from everywhere.  
To delete the linky simply run `npm unlink`

# Usage

## Importing

To import a component you must have installed the component as a common npm package first.

`cit import @namespace/my-component`

When you the import component, it copies components source code to a root component folder (a new `./components` folder in the root level of project is created - the same level as `src` folder).

It symlinks a npm package to the root `./components` (in the project `package.json` file a component's version is replaced by the path to the component's source in the root `./components` folder).

Now it's the time to do changes in the component's source code.

## Tagging

When you decide you are done with the changes, just tag the component with a new version:

`cit tag 0.0.8 @namespace/my-component`

## Publishing

To publish changes to the npm repository run:

`cit publish @namespace/my-component`

## Clearing - time to use npm package again

If you decide not to publish changes in component or you have tagged new version and published the component to npm repository, by calling a clear command you delete the components source code from the root `./components` folder and hook up yourself again to the proper npm package:

`cit clear @namespace/my-component`

# Downloading package source code

When you need to download package source code, it is possible by the command:

`cit download @namespace/my-component`

It will create a `my-component` folder and copy the package's content to it.

# License

This code is published under the MIT license, you can do whatever you want with it, but it is provided with absolutely no warranty.
